/*
 * Copyright (c) 2019 Axiomtek Co., Ltd.
 *
 * SPDX-License-Identifier:	GPL-2.0+
 */

#ifndef AXIO_CPLD_H
#define AXIO_CPLD_H

int axio_cpld_common_probe(struct device *dev, struct regmap *regmap, int irq,
		      const char *name);
void axio_cpld_common_remove(struct device *dev);

#endif  /* AXIO_CPLD_H */
