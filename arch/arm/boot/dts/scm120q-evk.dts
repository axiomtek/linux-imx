/*
 * Copyright (C) 2017 Axiomtek Co., Ltd.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */

#include "scm120q.dts"

/ {
	model = "Axiomtek i.MX6 Quad/Dual SCM120-120-EVK";
	compatible = "fsl,imx6q", "axiomtek,scm120";

	sound {
		compatible = "fsl,imx6q-sabresd-sgtl5000",
			   "fsl,imx-audio-sgtl5000";
		model = "sgtl5000";
		ssi-controller = <&ssi1>;
		audio-codec = <&codec>;
		audio-routing =
			"MIC_IN", "Mic Jack",
			"Mic Jack", "Mic Bias",
			"Headphone Jack", "HP_OUT";
		mux-int-port = <1>;
		mux-ext-port = <3>;
	};

	gpio-keys {
		sw1 {
			label = "SW1";
			gpios = <&gpio1 11 GPIO_ACTIVE_LOW>;
			linux,code = <KEY_VOLUMEUP>;
		};

		sw2 {
			label = "SW2";
			gpios = <&gpio1 10 GPIO_ACTIVE_LOW>;
			linux,code = <KEY_VOLUMEDOWN>;
		};

		sw3 {
			label = "SW3";
			gpios = <&gpio1 15 GPIO_ACTIVE_LOW>;
			linux,code = <KEY_A>;
		};

		sw4 {
			label = "SW4";
			gpios = <&gpio1 14 GPIO_ACTIVE_LOW>;
			linux,code = <KEY_B>;
		};

		sw5 {
			label = "SW5";
			gpios = <&gpio1 13 GPIO_ACTIVE_LOW>;
			linux,code = <KEY_C>;
		};

		sw6 {
			label = "SW6";
			gpios = <&gpio1 12 GPIO_ACTIVE_LOW>;
			linux,code = <KEY_D>;
		};

		sw7 {
			label = "SW7";
			gpios = <&gpio2 4 GPIO_ACTIVE_LOW>;
			linux,code = <KEY_E>;
		};

		sw8 {
			label = "SW8";
			gpios = <&gpio2 5 GPIO_ACTIVE_LOW>;
			linux,code = <KEY_F>;
		};
	};
};

&i2c1 {
        codec: sgtl5000@0a {
                compatible = "fsl,sgtl5000";
                reg = <0x0a>;
                clocks = <&clks IMX6QDL_CLK_CKO>;
                VDDA-supply = <&reg_2p5v>;
                VDDIO-supply = <&reg_3p3v>;
        };

/*
        eeprom@51 {
                compatible = "at24,24c02";
                reg = <0x51>;
                pagesize = <8>;
        };
*/
};

&ecspi1 {
	status = "okay";

	flash: m25p80@0 {
		#address-cells = <1>;
		#size-cells = <1>;
		compatible = "spansion,s25fl016k", "jedec,spi-nor";
		spi-max-frequency = <20000000>;
		reg = <0>;
	};
};

&sata {
	status = "okay";
};
