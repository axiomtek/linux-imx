/*
 * Copyright (C) 2018-2019 Axiomtek Co., Ltd.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */

#include "scm120q-evk.dts"

/ {

	sound-hdmi {
        	status = "disabled";
	};
};

&mxcfb1 {
        disp_dev = "ldb";
        interface_pix_fmt = "RGB24";
        default_bpp = <16>;
        status = "okay";
};

&mxcfb2 {
        status = "okay";
};

&hdmi_audio {
        status = "disabled";
};

&clks {
        fsl,ldb-di0-parent = <&clks IMX6QDL_CLK_PLL2_PFD0_352M>;
        fsl,ldb-di1-parent = <&clks IMX6QDL_CLK_PLL2_PFD0_352M>;
};

&ldb {
        status = "okay";

        lvds-channel@0 {
                fsl,data-mapping = "spwg";
                fsl,data-width = <24>;
                crtc = "ipu2-di0";
                primary;
                status = "okay";

                display-timings {
                        native-mode = <&timing0>;
                        timing0: g084sn05 {
                                clock-frequency = <40000000>;
                                hactive = <800>;
                                vactive = <600>;
                                hback-porch = <75>;
                                hfront-porch = <75>;
                                vback-porch = <7>;
                                vfront-porch = <75>;
                                hsync-len = <7>;
                                vsync-len = <7>;
                        };
                };
        };

        lvds-channel@1 {
                fsl,data-mapping = "spwg";
                fsl,data-width = <18>;
                crtc = "ipu2-di1";
                status = "okay";

                display-timings {
                        native-mode = <&timing1>;
                        timing1: hsd100pxn1 {
                                clock-frequency = <65000000>;
                                hactive = <1024>;
                                vactive = <768>;
                                hback-porch = <220>;
                                hfront-porch = <40>;
                                vback-porch = <21>;
                                vfront-porch = <7>;
                                hsync-len = <60>;
                                vsync-len = <10>;
                        };
                };
        };
};
