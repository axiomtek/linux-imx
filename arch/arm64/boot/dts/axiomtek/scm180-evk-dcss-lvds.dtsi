/*
 * Copyright 2019 Axiomtek Co., Ltd.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

/ {
	sound-hdmi {
		status = "disabled";
	};

	leds {
		compatible = "gpio-leds";
		pinctrl-names = "default";
		pinctrl-0 = <&pinctrl_lcd_pwr_en>;

		led0: pwr_en {
			label = "lcd_pwr_en";
			gpios = <&gpio4 1 GPIO_ACTIVE_HIGH>;
			default-state = "on";
			linux,default-trigger = "default-on";
		};
	};
};

&i2c3 {
        dsi_lvds_bridge: sn65dsi84@2c {
		compatible = "ti,sn65dsi83";
		pinctrl-names = "default";
		pinctrl-0 = <&pinctrl_lvds_en>;
		reg = <0x2c>;
		ti,dsi-lanes = <4>;
		ti,lvds-format = <1>;
		ti,lvds-bpp = <24>;
		ti,width-mm = <170>;
		ti,height-mm = <128>;
		/* MIPI_DSI_MODE_VIDEO | MIPI_DSI_CLOCK_NON_CONTINUOUS */
		ti,dsi-mode-flags = <0x401>;
		enable-gpios = <&gpio3 12 GPIO_ACTIVE_HIGH>;
		status = "okay";

		display-timings {
			lvds {
				/* AUO g084sn05 */
				clock-frequency = <40000000>;
				hactive = <800>;
				vactive = <600>;
				hback-porch = <0>;
				hfront-porch = <246>;
				vback-porch = <0>;
				vfront-porch = <26>;
				hsync-len = <10>;
				vsync-len = <2>;
				hsync-active = <0>;
				vsync-active = <0>;
				de-active = <1>;
			};
		};

		port {
			sn65dsi84_in: endpoint {
				remote-endpoint = <&mipi_dsi_bridge_out>;
			};
		};
        };
};

&hdmi {
	status = "disabled";
};

&dcss {
	status = "okay";
	disp-dev = "mipi_disp";

	clocks = <&clk IMX8MQ_CLK_DISP_APB_ROOT>,
		 <&clk IMX8MQ_CLK_DISP_AXI_ROOT>,
                 <&clk IMX8MQ_CLK_DISP_RTRM_ROOT>,
                 <&clk IMX8MQ_CLK_DC_PIXEL>,
                 <&clk IMX8MQ_CLK_DISP_DTRC>;
	clock-names = "apb", "axi", "rtrm", "pix", "dtrc";

	assigned-clocks = <&clk IMX8MQ_CLK_DC_PIXEL>,
                          <&clk IMX8MQ_CLK_DISP_AXI>,
                          <&clk IMX8MQ_CLK_DISP_RTRM>;
	assigned-clock-parents = <&clk IMX8MQ_VIDEO_PLL1_OUT>,
                                 <&clk IMX8MQ_SYS1_PLL_800M>,
                                 <&clk IMX8MQ_SYS1_PLL_800M>;
	assigned-clock-rates = <600000000>,
                               <800000000>,
                               <400000000>;

	dcss_disp0: port@0 {
		reg = <0>;

		dcss_disp0_mipi_dsi: mipi_dsi {
			remote-endpoint = <&mipi_dsi_in>;
		};
	};
};

&mipi_dsi_phy {
	status = "okay";
};

&mipi_dsi {
	status = "okay";

	port@1 {
		mipi_dsi_in: endpoint {
			remote-endpoint = <&dcss_disp0_mipi_dsi>;
		};
	};
};

&mipi_dsi_bridge {
	status = "okay";
	clock-drop-level = <4>;

	port@2 {
		mipi_dsi_bridge_out: endpoint {
			remote-endpoint = <&sn65dsi84_in>;
		};
	};
};

&iomuxc {
	scm180 {
		pinctrl_mipi_dsi_en: mipi_dsi_en {
			fsl,pins = <
				MX8MQ_IOMUXC_SPDIF_TX_GPIO5_IO3 	0x16
			>;
		};

		pinctrl_lcd_pwr_en: lcd_pwr_en {
			fsl,pins = <
				MX8MQ_IOMUXC_SAI1_RXC_GPIO4_IO1 	0x16
			>;
		};

		pinctrl_lvds_en: lvds_en {
			fsl,pins = <
				MX8MQ_IOMUXC_NAND_DATA06_GPIO3_IO12 	0x16
			>;
		};
	};
};
